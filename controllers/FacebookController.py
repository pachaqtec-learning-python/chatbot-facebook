import logging
import json
from flask import request

from utils.environment import Environment

from controllers.FootBallController import FootBallController
from controllers.FacebookActionController import FacebookActionController

class FacebookController():

    def __init__(self):
        # Instanciando Clases
        self.actionFootball = FootBallController()
        self.actionFacebook = FacebookActionController()
        self.env = Environment()

    def validate(self):
        # Obtener las variables de entorno para facebook.
        envFacebook = self.env.facebook()

        # Almacenamos los valores enviados como parametros en el request
        valueHubMode = request.args.get("hub.mode")
        valueHubChallenge = request.args.get("hub.challenge")
        valueHubToken = request.args.get("hub.verify_token")

        if valueHubMode == "subscribe" and valueHubChallenge:
            # Validar que el Token enviando por facebook sea idéntico al que
            # tenemos en nuestro servidor. Si es invalido devolvemos un status
            # code 403. Caso contrario devolvemos un status code 200 con el
            # valor hub.challenge
            if not valueHubToken == envFacebook['TOKEN']:
                return "Verification token mismatch", 403

            return valueHubChallenge, 200

    def receivedMessage(self):
        data = request.get_json()

        # Validar Type Message
        if data["object"] == "page":
            for entry in data["entry"]:
                for messagingEvent in entry["messaging"]:
                    recipientID = messagingEvent["sender"]["id"]

                    # Alguien envía un mensaje
                    if messagingEvent.get("message"):
                        message_text = messagingEvent["message"]["text"]

                        if 'quick_reply' in messagingEvent['message']:
                            payload = messagingEvent['message']['quick_reply']['payload']
                            self.controlPayload(recipientID, payload, message_text )
                        else:
                            self.actionFacebook.sendAction(recipientID, 'mark_seen')
                            self.actionFacebook.sendAction(recipientID, 'typing_on')
                            self.actionFacebook.sendMessage(recipientID, message_text)

                    # Evento cuando usuario hace click en botones
                    if messagingEvent.get("postback"):
                        payload = messagingEvent["postback"]['payload']
                        title = messagingEvent["postback"]['title']
                        self.controlPayload(recipientID, payload, title)

                    # Evento cuando usuario hace click en botones
                    if messagingEvent.get("delivery"):
                        self.actionFacebook.sendAction(recipientID, 'typing_off')

        return 'OK', 200

    def controlPayload(self, recipientID, payload, title):
        self.actionFacebook.sendAction(recipientID, 'typing_on')

        if payload.upper() == 'LIST_COUNTRIES':
            self.responseListCountries(recipientID)

        if payload.upper() == 'LIST_LEAGUES':
            self.responseListLeagues(recipientID, title.lower())


    def responseListCountries(self, recipientID):
        buttons = []
        for country in self.actionFootball.listCountries():
            buttons.append(
                {
                    "content_type":"text",
                    "title":country['country'],
                    "payload":"LIST_LEAGUES",
                    "image_url":country['flag']
                }
            )
            if len(buttons) >=13:
                break

        templateData = {
            "recipientID": recipientID,
            "text": "Seleccione el país: ",
            "quickReplies": buttons
        }
        return self.actionFacebook.sendQuickReplies(templateData)


    def responseListLeagues(self, recipientID, country):
        buttons = []
        for league in self.actionFootball.listLeagues(country):
            buttons.append(
                {
                    "content_type":"text",
                    "title":league['name'],
                    "payload":"LIST_TEAMS",
                    "image_url":league['logo']
                }
            )
            if len(buttons) >=13:
                break

        templateData = {
            "recipientID": recipientID,
            "text": "Seleccione la liga: ",
            "quickReplies": buttons
        }
        return self.actionFacebook.sendQuickReplies(templateData)




