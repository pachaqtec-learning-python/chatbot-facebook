import logging
from flask import Flask
from utils.environment import Environment

def createAPP():
    app = Flask(__name__)

    # Routes de Facebook
    from routes.facebookRoutes import facebookBP
    app.register_blueprint(facebookBP, url_prefix='/facebook')

    # Routes de Facebook para su configuración
    from routes.facebookRoutes import facebookConfigBP
    app.register_blueprint(facebookConfigBP, url_prefix='/facebook/config')

    # Routes de Twitter
    from routes.twitterRoutes import twitterBP
    app.register_blueprint(twitterBP, url_prefix='/twitter')

    # Routes de Football
    from routes.footballRoutes import footballBP
    app.register_blueprint(footballBP, url_prefix='/football')

    return app

def initializeLogging():
    # https://realpython.com/python-logging/
    logging.basicConfig(
        filename='app.log',
        filemode='w',
        level = logging.DEBUG,
        format='[%(asctime)s ][%(levelname)s][%(filename)s] : %(message)s',
        datefmt='%d-%b-%y %H:%M:%S'
)

if __name__ == '__main__':
    # Instanciar la clase Environment
    env = Environment()

    # Inicializar la configuración de logging
    initializeLogging()

    # Obtener la variables de entornos para el proyecto
    config = env.general()

    app = createAPP()
    app.run(port = config['PORT'], debug = True)
