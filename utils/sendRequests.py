import logging
import requests
import json

from utils.environment import Environment as Env


class SendRequests(Env):
    def __init__(self):
        self.URL_API = None
        self.params = None
        self.headers = None

    def get(self, service, endpoint, data = None):
        self.optionConfig(service)
        urlComplet = f'{self.URL_API}/{endpoint}'

        response = requests.get(
            urlComplet,
            params=self.params,
            headers=self.headers,
            data=json.dumps(data)
        )

        if response.status_code != 200:
            logging.error(' --- SEND REQUEST ---')
            logging.error(f' método : GET')
            logging.error(f' endpoint : {urlComplet} ')
            logging.error(f' params : {self.params} ')
            logging.error(f' headers : {self.headers} ')
            logging.error(f' data : {json.dumps(data)} ')
            logging.error(f' status_code : {response.status_code}')
            logging.error(f' response_text : {response.text}')
        else:
            return response.text

    def delete(self, service, endpoint, data = None):
        self.optionConfig(service)
        urlComplet = f'{self.URL_API}/{endpoint}'

        response = requests.delete(
            urlComplet,
            params=self.params,
            headers=self.headers,
            data=json.dumps(data)
        )

        if response.status_code != 200:
            logging.error(' --- SEND REQUEST ---')
            logging.error(f' método : DELETE')
            logging.error(f' endpoint : {urlComplet} ')
            logging.error(f' params : {self.params} ')
            logging.error(f' headers : {self.headers} ')
            logging.error(f' data : {json.dumps(data)} ')
            logging.error(f' status_code : {response.status_code}')
            logging.error(f' response_text : {response.text}')
        else:
            return response.text

    def post(self, service, endpoint, data = None):
        self.optionConfig(service)
        urlComplet = f'{self.URL_API}/{endpoint}'

        response = requests.post(
            urlComplet,
            params=self.params,
            headers=self.headers,
            data=json.dumps(data)
        )

        if response.status_code != 200:
            logging.error(' --- SEND REQUEST ---')
            logging.error(f' método : POST')
            logging.error(f' endpoint : {urlComplet} ')
            logging.error(f' params : {self.params} ')
            logging.error(f' headers : {self.headers} ')
            logging.error(f' data : {json.dumps(data)} ')
            logging.error(f' status_code : {response.status_code}')
            logging.error(f' response_text : {response.text}')
        else:
            return response.text

    def optionConfig(self, service):
        if service.upper() == 'FACEBOOK':
            self.configFacebook()

        if service.upper() == 'FOOTBALL':
            self.configFootBall()

    def configFacebook(self):
        # Almacenamos las variables env de conexión a Facebook
        envFacebook = self.facebook()
        ACCESS_TOKEN = envFacebook['ACCESS_TOKEN']
        self.URL_API = envFacebook['URL_API']

        # Configuramos los parametros generales para el envió
        # de información hacía Facebook
        self.params = { "access_token": ACCESS_TOKEN }
        self.headers = { "Content-Type": "application/json" }

    def configFootBall(self):
        # Almacenamos las variables env de conexión a Facebook
        envFootball = self.footBall()
        self.URL_API = envFootball['URL_API']

        # Configuramos los parametros generales para el envió
        # de información hacía Football
        self. headers = {
            'x-rapidapi-host': envFootball['X-RAPIDAPI-HOST'],
            'x-rapidapi-key': envFootball['X-RAPIDAPI-KEY']
        }
